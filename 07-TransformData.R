cat("Transforming data...\n")

# Your code goes here

# Purge obsolete variables
rm()

# Garbage collection
gc()

cat("Data transformed!\n")
