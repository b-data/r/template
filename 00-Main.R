# Declare necessary packages
pkgs_cran <- c("config")
pkgs_bioc <- c()

# Load packages
source("01-LoadPkgs.R")

# Read config
source("02-ReadConfig.R")

# Initialise
source("03-Init.R")

# Load functions
source("04-LoadFunc.R")

# Wrangle
## Import
source("05-ImportData.R")
## Tidy
source("06-TidyData.R")

# Wrangle, Explore
## Transform
source("07-TransformData.R")

# Explore
## Vizualise
source("08-VisualiseData.R")
## Model
source("09-ModelData.R")

# Communicate
## Save Output
source("10-SaveOutput.R")
