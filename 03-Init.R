cat("Initialising...\n")

# Delete all objects
rm(list = setdiff(ls(), c("cfg", "pkgs_cran", "pkgs_bioc")))

# Get user
if (.Platform$OS.type == "windows") {
  user <- Sys.getenv("USERNAME")
} else {
  user <- Sys.getenv("USER")
}

# Define paths to use
idir <- file.path(cfg$idir_root, "input")
odir <- file.path(cfg$odir_root, "output", user)

# Create odir if not already existent
dir.create(odir, showWarnings = FALSE, recursive = TRUE)

# Delete all files and folders in odir
rmdirs <- dir(odir)
# if (length(rmdirs) > 0) {
#   unlink(file.path(odir, rmdirs), recursive = TRUE)
# }

# Purge obsolete variables
rm(user, rmdirs)

cat("Initialised!\n")
